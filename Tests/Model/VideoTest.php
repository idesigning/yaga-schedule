<?php
/**
 * VideoTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  YagaSchedule\Server\Tests\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Yaga order gateway
 *
 * Common schema for Yaga schedule requests. Swagger
 *
 * OpenAPI spec version: 18.40.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace YagaSchedule\Server\Model;

/**
 * VideoTest Class Doc Comment
 *
 * @category    Class */
// * @description Видео  url      (*) - ссылка на видео type     (*) - тип видео language (*) - язык видео
/**
 * @package     YagaSchedule\Server\Tests\Model
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class VideoTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Video"
     */
    public function testVideo()
    {
        $testVideo = new Video();
    }

    /**
     * Test attribute "url"
     */
    public function testPropertyUrl()
    {
    }

    /**
     * Test attribute "type"
     */
    public function testPropertyType()
    {
    }

    /**
     * Test attribute "language"
     */
    public function testPropertyLanguage()
    {
    }
}

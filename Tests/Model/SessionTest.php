<?php
/**
 * SessionTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  YagaSchedule\Server\Tests\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Yaga order gateway
 *
 * Common schema for Yaga schedule requests. Swagger
 *
 * OpenAPI spec version: 18.40.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace YagaSchedule\Server\Model;

/**
 * SessionTest Class Doc Comment
 *
 * @category    Class */
// * @description Сеанс.  Это сущность, характеризующая конкретное событие, время и место провередения. Например, для кино - это киносеанс.  id                  (*) - идентификатор сеанса venue_id            (*) - идентификатор площадки (Venue) event_id            (*) - идентификатор события (Event) hall_id             (*) - идентификатор зала (Hall) organizer_id            - идентификатор организатора (Organizer) session_time        (*) - время сеанса tags                    - список произвольных тегов sale_opening            - дата/время открытия продаж sale_closing            - дата/время закрытия продаж fee_percent             - процент сервисного сбора (по-умолчанию - 0) discount_percent        - процент скидки, предоставляемой партнером (по-умолчанию - 0) formats                 - список форматов фильма (для кино) sale_supported      (*) - разрешены ли продажи на данный сеанс prices                  - список цен can_pre_order           - разрешено ли создание пред-заказа available_seat_count    - число доступных для продажи мест additional              - служебное поле
/**
 * @package     YagaSchedule\Server\Tests\Model
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SessionTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Session"
     */
    public function testSession()
    {
        $testSession = new Session();
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "venueId"
     */
    public function testPropertyVenueId()
    {
    }

    /**
     * Test attribute "eventId"
     */
    public function testPropertyEventId()
    {
    }

    /**
     * Test attribute "hallId"
     */
    public function testPropertyHallId()
    {
    }

    /**
     * Test attribute "organizerId"
     */
    public function testPropertyOrganizerId()
    {
    }

    /**
     * Test attribute "sessionTime"
     */
    public function testPropertySessionTime()
    {
    }

    /**
     * Test attribute "tags"
     */
    public function testPropertyTags()
    {
    }

    /**
     * Test attribute "saleOpening"
     */
    public function testPropertySaleOpening()
    {
    }

    /**
     * Test attribute "saleClosing"
     */
    public function testPropertySaleClosing()
    {
    }

    /**
     * Test attribute "feePercent"
     */
    public function testPropertyFeePercent()
    {
    }

    /**
     * Test attribute "discountPercent"
     */
    public function testPropertyDiscountPercent()
    {
    }

    /**
     * Test attribute "formats"
     */
    public function testPropertyFormats()
    {
    }

    /**
     * Test attribute "saleSupported"
     */
    public function testPropertySaleSupported()
    {
    }

    /**
     * Test attribute "prices"
     */
    public function testPropertyPrices()
    {
    }

    /**
     * Test attribute "canPreOrder"
     */
    public function testPropertyCanPreOrder()
    {
    }

    /**
     * Test attribute "availableSeatCount"
     */
    public function testPropertyAvailableSeatCount()
    {
    }

    /**
     * Test attribute "additional"
     */
    public function testPropertyAdditional()
    {
    }
}

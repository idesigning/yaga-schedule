# YagaSchedule\Server\Api\YagaScheduleApiInterface

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCities**](YagaScheduleApiInterface.md#getCities) | **GET** /cities | 
[**getEvents**](YagaScheduleApiInterface.md#getEvents) | **GET** /events | 
[**getHallplan**](YagaScheduleApiInterface.md#getHallplan) | **GET** /hallplan | 
[**getHalls**](YagaScheduleApiInterface.md#getHalls) | **GET** /halls | 
[**getManifest**](YagaScheduleApiInterface.md#getManifest) | **GET** /manifest | 
[**getOrganizers**](YagaScheduleApiInterface.md#getOrganizers) | **GET** /organizers | 
[**getPersons**](YagaScheduleApiInterface.md#getPersons) | **GET** /persons | 
[**getSchedule**](YagaScheduleApiInterface.md#getSchedule) | **GET** /schedule | 
[**getVenues**](YagaScheduleApiInterface.md#getVenues) | **GET** /venues | 


## Service Declaration
```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.yagaSchedule:
        class: Acme\MyBundle\Api\YagaScheduleApi
        tags:
            - { name: "swagger_server.api", api: "yagaSchedule" }
    # ...
```

## **getCities**
> YagaSchedule\Server\Model\GetCitiesResult getCities($offset, $limit, $cityId, $updatedAfter)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaScheduleApiInterface.php

namespace Acme\MyBundle\Api;

use YagaSchedule\Server\Api\YagaScheduleApiInterface;

class YagaScheduleApi implements YagaScheduleApiInterface
{

    // ...

    /**
     * Implementation of YagaScheduleApiInterface#getCities
     */
    public function getCities($offset = null, $limit = null, array $cityId = null, \DateTime $updatedAfter = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string**|  | [optional]
 **limit** | **string**|  | [optional]
 **cityId** | [**string**](../Model/string.md)|  | [optional]
 **updatedAfter** | **\DateTime**|  | [optional]

### Return type

[**YagaSchedule\Server\Model\GetCitiesResult**](../Model/GetCitiesResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getEvents**
> YagaSchedule\Server\Model\GetEventsResult getEvents($offset, $limit, $eventId, $dateFrom, $dateTo, $updatedAfter)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaScheduleApiInterface.php

namespace Acme\MyBundle\Api;

use YagaSchedule\Server\Api\YagaScheduleApiInterface;

class YagaScheduleApi implements YagaScheduleApiInterface
{

    // ...

    /**
     * Implementation of YagaScheduleApiInterface#getEvents
     */
    public function getEvents($offset = null, $limit = null, array $eventId = null, \DateTime $dateFrom = null, \DateTime $dateTo = null, \DateTime $updatedAfter = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string**|  | [optional]
 **limit** | **string**|  | [optional]
 **eventId** | [**string**](../Model/string.md)|  | [optional]
 **dateFrom** | **\DateTime**|  | [optional]
 **dateTo** | **\DateTime**|  | [optional]
 **updatedAfter** | **\DateTime**|  | [optional]

### Return type

[**YagaSchedule\Server\Model\GetEventsResult**](../Model/GetEventsResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getHallplan**
> YagaSchedule\Server\Model\GetHallplanResult getHallplan($sessionId)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaScheduleApiInterface.php

namespace Acme\MyBundle\Api;

use YagaSchedule\Server\Api\YagaScheduleApiInterface;

class YagaScheduleApi implements YagaScheduleApiInterface
{

    // ...

    /**
     * Implementation of YagaScheduleApiInterface#getHallplan
     */
    public function getHallplan($sessionId = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionId** | **string**|  | [optional]

### Return type

[**YagaSchedule\Server\Model\GetHallplanResult**](../Model/GetHallplanResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getHalls**
> YagaSchedule\Server\Model\GetHallsResult getHalls($offset, $limit, $venueId, $hallId, $updatedAfter)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaScheduleApiInterface.php

namespace Acme\MyBundle\Api;

use YagaSchedule\Server\Api\YagaScheduleApiInterface;

class YagaScheduleApi implements YagaScheduleApiInterface
{

    // ...

    /**
     * Implementation of YagaScheduleApiInterface#getHalls
     */
    public function getHalls($offset = null, $limit = null, $venueId = null, array $hallId = null, \DateTime $updatedAfter = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string**|  | [optional]
 **limit** | **string**|  | [optional]
 **venueId** | **string**|  | [optional]
 **hallId** | [**string**](../Model/string.md)|  | [optional]
 **updatedAfter** | **\DateTime**|  | [optional]

### Return type

[**YagaSchedule\Server\Model\GetHallsResult**](../Model/GetHallsResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getManifest**
> YagaSchedule\Server\Model\GetManifestResult getManifest()



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaScheduleApiInterface.php

namespace Acme\MyBundle\Api;

use YagaSchedule\Server\Api\YagaScheduleApiInterface;

class YagaScheduleApi implements YagaScheduleApiInterface
{

    // ...

    /**
     * Implementation of YagaScheduleApiInterface#getManifest
     */
    public function getManifest()
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**YagaSchedule\Server\Model\GetManifestResult**](../Model/GetManifestResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getOrganizers**
> YagaSchedule\Server\Model\GetOrganizersResult getOrganizers($offset, $limit, $organizerId, $updatedAfter)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaScheduleApiInterface.php

namespace Acme\MyBundle\Api;

use YagaSchedule\Server\Api\YagaScheduleApiInterface;

class YagaScheduleApi implements YagaScheduleApiInterface
{

    // ...

    /**
     * Implementation of YagaScheduleApiInterface#getOrganizers
     */
    public function getOrganizers($offset = null, $limit = null, array $organizerId = null, \DateTime $updatedAfter = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string**|  | [optional]
 **limit** | **string**|  | [optional]
 **organizerId** | [**string**](../Model/string.md)|  | [optional]
 **updatedAfter** | **\DateTime**|  | [optional]

### Return type

[**YagaSchedule\Server\Model\GetOrganizersResult**](../Model/GetOrganizersResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getPersons**
> YagaSchedule\Server\Model\GetPersonsResult getPersons($offset, $limit, $personId, $updatedAfter)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaScheduleApiInterface.php

namespace Acme\MyBundle\Api;

use YagaSchedule\Server\Api\YagaScheduleApiInterface;

class YagaScheduleApi implements YagaScheduleApiInterface
{

    // ...

    /**
     * Implementation of YagaScheduleApiInterface#getPersons
     */
    public function getPersons($offset = null, $limit = null, array $personId = null, \DateTime $updatedAfter = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string**|  | [optional]
 **limit** | **string**|  | [optional]
 **personId** | [**string**](../Model/string.md)|  | [optional]
 **updatedAfter** | **\DateTime**|  | [optional]

### Return type

[**YagaSchedule\Server\Model\GetPersonsResult**](../Model/GetPersonsResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getSchedule**
> YagaSchedule\Server\Model\GetScheduleResult getSchedule($offset, $limit, $venueId, $sessionId, $updatedAfter)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaScheduleApiInterface.php

namespace Acme\MyBundle\Api;

use YagaSchedule\Server\Api\YagaScheduleApiInterface;

class YagaScheduleApi implements YagaScheduleApiInterface
{

    // ...

    /**
     * Implementation of YagaScheduleApiInterface#getSchedule
     */
    public function getSchedule($offset = null, $limit = null, $venueId = null, array $sessionId = null, \DateTime $updatedAfter = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string**|  | [optional]
 **limit** | **string**|  | [optional]
 **venueId** | **string**|  | [optional]
 **sessionId** | [**string**](../Model/string.md)|  | [optional]
 **updatedAfter** | **\DateTime**|  | [optional]

### Return type

[**YagaSchedule\Server\Model\GetScheduleResult**](../Model/GetScheduleResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

## **getVenues**
> YagaSchedule\Server\Model\GetVenuesResult getVenues($offset, $limit, $cityId, $venueId, $updatedAfter)



### Example Implementation
```php
<?php
// src/Acme/MyBundle/Api/YagaScheduleApiInterface.php

namespace Acme\MyBundle\Api;

use YagaSchedule\Server\Api\YagaScheduleApiInterface;

class YagaScheduleApi implements YagaScheduleApiInterface
{

    // ...

    /**
     * Implementation of YagaScheduleApiInterface#getVenues
     */
    public function getVenues($offset = null, $limit = null, $cityId = null, array $venueId = null, \DateTime $updatedAfter = null)
    {
        // Implement the operation ...
    }

    // ...
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **string**|  | [optional]
 **limit** | **string**|  | [optional]
 **cityId** | **string**|  | [optional]
 **venueId** | [**string**](../Model/string.md)|  | [optional]
 **updatedAfter** | **\DateTime**|  | [optional]

### Return type

[**YagaSchedule\Server\Model\GetVenuesResult**](../Model/GetVenuesResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


# GetHallsResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**halls** | [**YagaSchedule\Server\Model\Hall**](Hall.md) |  | [optional] 
**paging** | [**YagaSchedule\Server\Model\ListInfo**](ListInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# City

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**coordinates** | [**YagaSchedule\Server\Model\Coordinates**](Coordinates.md) |  | [optional] 
**timezone** | **string** |  | [optional] 
**integrations** | [**YagaSchedule\Server\Model\SourceRef**](SourceRef.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



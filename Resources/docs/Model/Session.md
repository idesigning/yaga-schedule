# Session

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**venueId** | **string** |  | [optional] 
**eventId** | **string** |  | [optional] 
**hallId** | **string** |  | [optional] 
**organizerId** | **string** |  | [optional] 
**sessionTime** | [**YagaSchedule\Server\Model\SessionTime**](SessionTime.md) |  | [optional] 
**tags** | [**YagaSchedule\Server\Model\Tag**](Tag.md) |  | [optional] 
**saleOpening** | [**\DateTime**](\DateTime.md) |  | [optional] 
**saleClosing** | [**\DateTime**](\DateTime.md) |  | [optional] 
**feePercent** | **double** |  | [optional] 
**discountPercent** | **double** |  | [optional] 
**formats** | [**YagaSchedule\Server\Model\MovieFormat**](MovieFormat.md) |  | [optional] 
**saleSupported** | **bool** |  | [optional] 
**prices** | [**YagaSchedule\Server\Model\Money**](Money.md) |  | [optional] 
**canPreOrder** | [**YagaSchedule\Server\Model\PreOrderAllowance**](PreOrderAllowance.md) |  | [optional] 
**availableSeatCount** | **int** |  | [optional] 
**additional** | **array** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# ListInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**limit** | **string** |  | [optional] 
**offset** | **string** |  | [optional] 
**total** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



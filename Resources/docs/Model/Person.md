# Person

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**type** | [**YagaSchedule\Server\Model\PersonType**](PersonType.md) |  | [optional] 
**initDate** | [**\DateTime**](\DateTime.md) |  | [optional] 
**endDate** | [**\DateTime**](\DateTime.md) |  | [optional] 
**briefDescription** | **string** |  | [optional] 
**fullDescription** | **string** |  | [optional] 
**synonyms** | **string** |  | [optional] 
**tags** | [**YagaSchedule\Server\Model\Tag**](Tag.md) |  | [optional] 
**image** | [**YagaSchedule\Server\Model\Image**](Image.md) |  | [optional] 
**integrations** | [**YagaSchedule\Server\Model\SourceRef**](SourceRef.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



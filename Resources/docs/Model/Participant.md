# Participant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**roles** | [**YagaSchedule\Server\Model\Role**](Role.md) |  | [optional] 
**integrations** | [**YagaSchedule\Server\Model\SourceRef**](SourceRef.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# SessionTime

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionStart** | [**\DateTime**](\DateTime.md) |  | [optional] 
**sessionEnd** | [**\DateTime**](\DateTime.md) |  | [optional] 
**type** | [**YagaSchedule\Server\Model\SessionType**](SessionType.md) |  | [optional] 
**timezone** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



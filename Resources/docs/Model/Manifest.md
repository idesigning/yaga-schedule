# Manifest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**citiesUpdateTime** | [**\DateTime**](\DateTime.md) |  | [optional] 
**venuesUpdateTime** | [**\DateTime**](\DateTime.md) |  | [optional] 
**eventsUpdateTime** | [**\DateTime**](\DateTime.md) |  | [optional] 
**hallsUpdateTime** | [**\DateTime**](\DateTime.md) |  | [optional] 
**schedulesUpdateTime** | [**\DateTime**](\DateTime.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



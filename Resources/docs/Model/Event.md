# Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**type** | [**YagaSchedule\Server\Model\EventType**](EventType.md) |  | [optional] 
**ageRestriction** | **int** |  | [optional] 
**duration** | **int** |  | [optional] 
**tags** | [**YagaSchedule\Server\Model\Tag**](Tag.md) |  | [optional] 
**participants** | [**YagaSchedule\Server\Model\Participant**](Participant.md) |  | [optional] 
**images** | [**YagaSchedule\Server\Model\Image**](Image.md) |  | [optional] 
**videos** | [**YagaSchedule\Server\Model\Video**](Video.md) |  | [optional] 
**urls** | **string** |  | [optional] 
**fullDescription** | **string** |  | [optional] 
**briefDescription** | **string** |  | [optional] 
**originalTitle** | **string** |  | [optional] 
**countries** | **string** |  | [optional] 
**productionYear** | **int** |  | [optional] 
**stageTheatre** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**releaseDate** | **string** |  | [optional] 
**nonStop** | **bool** |  | [optional] 
**organizer** | **string** |  | [optional] 
**permanent** | **bool** |  | [optional] 
**sourceLink** | **string** |  | [optional] 
**ratings** | [**YagaSchedule\Server\Model\Rating**](Rating.md) |  | [optional] 
**synonyms** | **string** |  | [optional] 
**integrations** | [**YagaSchedule\Server\Model\SourceRef**](SourceRef.md) |  | [optional] 
**additional** | **array** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



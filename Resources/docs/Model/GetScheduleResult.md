# GetScheduleResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessions** | [**YagaSchedule\Server\Model\Session**](Session.md) |  | [optional] 
**paging** | [**YagaSchedule\Server\Model\ListInfo**](ListInfo.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



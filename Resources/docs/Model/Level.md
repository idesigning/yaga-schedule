# Level

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**seats** | [**YagaSchedule\Server\Model\Seat**](Seat.md) |  | [optional] 
**seatCount** | **int** |  | [optional] 
**admission** | **bool** |  | [optional] 
**entranceId** | **string** |  | [optional] 
**entranceName** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



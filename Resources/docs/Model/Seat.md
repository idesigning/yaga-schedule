# Seat

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**row** | **string** |  | [optional] 
**place** | **string** |  | [optional] 
**fragment** | **string** |  | [optional] 
**x** | **int** |  | [optional] 
**y** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# GetManifestResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**manifest** | [**YagaSchedule\Server\Model\Manifest**](Manifest.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# Venue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**name** | **string** |  | [optional] 
**cityId** | **string** |  | [optional] 
**address** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**urls** | **string** |  | [optional] 
**images** | [**YagaSchedule\Server\Model\Image**](Image.md) |  | [optional] 
**videos** | [**YagaSchedule\Server\Model\Video**](Video.md) |  | [optional] 
**subwayStations** | **string** |  | [optional] 
**phones** | [**YagaSchedule\Server\Model\Phone**](Phone.md) |  | [optional] 
**workTimes** | [**YagaSchedule\Server\Model\WorkTime**](WorkTime.md) |  | [optional] 
**coordinates** | [**YagaSchedule\Server\Model\Coordinates**](Coordinates.md) |  | [optional] 
**synonyms** | **string** |  | [optional] 
**tags** | [**YagaSchedule\Server\Model\Tag**](Tag.md) |  | [optional] 
**types** | [**YagaSchedule\Server\Model\VenueType**](VenueType.md) |  | [optional] 
**cancelAllowance** | [**YagaSchedule\Server\Model\CancelAllowance**](CancelAllowance.md) |  | [optional] 
**saleOpening** | **string** |  | [optional] 
**saleClosing** | **string** |  | [optional] 
**saleCanceling** | **string** |  | [optional] 
**reservationTimeout** | **string** |  | [optional] 
**integrations** | [**YagaSchedule\Server\Model\SourceRef**](SourceRef.md) |  | [optional] 
**additional** | **array** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



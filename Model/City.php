<?php
/**
 * City
 *
 * PHP version 5
 *
 * @category Class
 * @package  YagaSchedule\Server\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Yaga order gateway
 *
 * Common schema for Yaga schedule requests. Swagger
 *
 * OpenAPI spec version: 18.40.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace YagaSchedule\Server\Model;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class representing the City model.
 *
 * Город  id          (*) - идентификатор города name        (*) - название города coordinates     - координаты города timezone        - таймзона (Europe/Moscow, Asia/Yekaterinburg и т.д.) integrations    - поле для внутреннего использования
 *
 * @package YagaSchedule\Server\Model
 * @author  Swagger Codegen team
 */
class City 
{
        /**
     * @var string|null
     * @SerializedName("id")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $id;

    /**
     * @var string|null
     * @SerializedName("name")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $name;

    /**
     * @var YagaSchedule\Server\Model\Coordinates|null
     * @SerializedName("coordinates")
     * @Assert\Type("YagaSchedule\Server\Model\Coordinates")
     * @Type("YagaSchedule\Server\Model\Coordinates")
     */
    protected $coordinates;

    /**
     * @var string|null
     * @SerializedName("timezone")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $timezone;

    /**
     * @var YagaSchedule\Server\Model\SourceRef[]|null
     * @SerializedName("integrations")
     * @Assert\All({
     *   @Assert\Type("YagaSchedule\Server\Model\SourceRef")
     * })
     * @Type("array<YagaSchedule\Server\Model\SourceRef>")
     */
    protected $integrations;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->name = isset($data['name']) ? $data['name'] : null;
        $this->coordinates = isset($data['coordinates']) ? $data['coordinates'] : null;
        $this->timezone = isset($data['timezone']) ? $data['timezone'] : null;
        $this->integrations = isset($data['integrations']) ? $data['integrations'] : null;
    }

    /**
     * Gets id.
     *
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets id.
     *
     * @param string|null $id
     *
     * @return $this
     */
    public function setId($id = null)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets name.
     *
     * @param string|null $name
     *
     * @return $this
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets coordinates.
     *
     * @return \YagaSchedule\Server\Model\Coordinates|null
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * Sets coordinates.
     *
     * @param YagaSchedule\Server\Model\Coordinates|null $coordinates
     *
     * @return $this
     */
    public function setCoordinates(Coordinates $coordinates = null)
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * Gets timezone.
     *
     * @return string|null
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Sets timezone.
     *
     * @param string|null $timezone
     *
     * @return $this
     */
    public function setTimezone($timezone = null)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Gets integrations.
     *
     * @return \YagaSchedule\Server\Model\SourceRef[]|null
     */
    public function getIntegrations()
    {
        return $this->integrations;
    }

    /**
     * Sets integrations.
     *
     * @param YagaSchedule\Server\Model\SourceRef[]|null $integrations
     *
     * @return $this
     */
    public function setIntegrations(SourceRef $integrations = null)
    {
        $this->integrations = $integrations;

        return $this;
    }
}



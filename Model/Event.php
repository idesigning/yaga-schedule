<?php
/**
 * Event
 *
 * PHP version 5
 *
 * @category Class
 * @package  YagaSchedule\Server\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Yaga order gateway
 *
 * Common schema for Yaga schedule requests. Swagger
 *
 * OpenAPI spec version: 18.40.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace YagaSchedule\Server\Model;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;

/**
 * Class representing the Event model.
 *
 * Событие. Мероприятие.  id              (*) - идентификатор события name            (*) - название события type                - тип события ageRestriction      - возрастное ограничение duration            - длительность (например для фильма) tags                - произвольные теги participants        - участники images              - связанные изображения/фотографии/картинки videos              - связанные видео urls                - full_description    - подробное описание brief_description   - краткое описание original_title      - название на языке оригинала countries           - список стран, учавствовавших в производстве фильма production_year     - stage_theatre       - state               - release_date        - non_stop            - является ли событие нонстопом (для кино) organizer           - организатор мероприятия permanent           - source_link         - ссылка на события на сайте партнера ratings             - рейтинги события synonyms            - синонимы события integrations        - служебное поле additional          - служебное поле
 *
 * @package YagaSchedule\Server\Model
 * @author  Swagger Codegen team
 */
class Event 
{
        /**
     * @var string|null
     * @SerializedName("id")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $id;

    /**
     * @var string|null
     * @SerializedName("name")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $name;

    /**
     * @var YagaSchedule\Server\Model\EventType|null
     * @SerializedName("type")
     * @Assert\Type("YagaSchedule\Server\Model\EventType")
     * @Type("YagaSchedule\Server\Model\EventType")
     */
    protected $type;

    /**
     * @var int|null
     * @SerializedName("ageRestriction")
     * @Assert\Type("int")
     * @Type("int")
     */
    protected $ageRestriction;

    /**
     * @var int|null
     * @SerializedName("duration")
     * @Assert\Type("int")
     * @Type("int")
     */
    protected $duration;

    /**
     * @var YagaSchedule\Server\Model\Tag[]|null
     * @SerializedName("tags")
     * @Assert\All({
     *   @Assert\Type("YagaSchedule\Server\Model\Tag")
     * })
     * @Type("array<YagaSchedule\Server\Model\Tag>")
     */
    protected $tags;

    /**
     * @var YagaSchedule\Server\Model\Participant[]|null
     * @SerializedName("participants")
     * @Assert\All({
     *   @Assert\Type("YagaSchedule\Server\Model\Participant")
     * })
     * @Type("array<YagaSchedule\Server\Model\Participant>")
     */
    protected $participants;

    /**
     * @var YagaSchedule\Server\Model\Image[]|null
     * @SerializedName("images")
     * @Assert\All({
     *   @Assert\Type("YagaSchedule\Server\Model\Image")
     * })
     * @Type("array<YagaSchedule\Server\Model\Image>")
     */
    protected $images;

    /**
     * @var YagaSchedule\Server\Model\Video[]|null
     * @SerializedName("videos")
     * @Assert\All({
     *   @Assert\Type("YagaSchedule\Server\Model\Video")
     * })
     * @Type("array<YagaSchedule\Server\Model\Video>")
     */
    protected $videos;

    /**
     * @var string[]|null
     * @SerializedName("urls")
     * @Assert\All({
     *   @Assert\Type("string")
     * })
     * @Type("array<string>")
     */
    protected $urls;

    /**
     * @var string|null
     * @SerializedName("fullDescription")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $fullDescription;

    /**
     * @var string|null
     * @SerializedName("briefDescription")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $briefDescription;

    /**
     * @var string|null
     * @SerializedName("originalTitle")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $originalTitle;

    /**
     * @var string[]|null
     * @SerializedName("countries")
     * @Assert\All({
     *   @Assert\Type("string")
     * })
     * @Type("array<string>")
     */
    protected $countries;

    /**
     * @var int|null
     * @SerializedName("productionYear")
     * @Assert\Type("int")
     * @Type("int")
     */
    protected $productionYear;

    /**
     * @var string|null
     * @SerializedName("stageTheatre")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $stageTheatre;

    /**
     * @var string|null
     * @SerializedName("state")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $state;

    /**
     * @var string|null
     * @SerializedName("releaseDate")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $releaseDate;

    /**
     * @var bool|null
     * @SerializedName("nonStop")
     * @Assert\Type("bool")
     * @Type("bool")
     */
    protected $nonStop;

    /**
     * @var string|null
     * @SerializedName("organizer")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $organizer;

    /**
     * @var bool|null
     * @SerializedName("permanent")
     * @Assert\Type("bool")
     * @Type("bool")
     */
    protected $permanent;

    /**
     * @var string|null
     * @SerializedName("sourceLink")
     * @Assert\Type("string")
     * @Type("string")
     */
    protected $sourceLink;

    /**
     * @var YagaSchedule\Server\Model\Rating[]|null
     * @SerializedName("ratings")
     * @Assert\All({
     *   @Assert\Type("YagaSchedule\Server\Model\Rating")
     * })
     * @Type("array<YagaSchedule\Server\Model\Rating>")
     */
    protected $ratings;

    /**
     * @var string[]|null
     * @SerializedName("synonyms")
     * @Assert\All({
     *   @Assert\Type("string")
     * })
     * @Type("array<string>")
     */
    protected $synonyms;

    /**
     * @var YagaSchedule\Server\Model\SourceRef[]|null
     * @SerializedName("integrations")
     * @Assert\All({
     *   @Assert\Type("YagaSchedule\Server\Model\SourceRef")
     * })
     * @Type("array<YagaSchedule\Server\Model\SourceRef>")
     */
    protected $integrations;

    /**
     * @var array|null
     * @SerializedName("additional")
     * @Assert\Type("array")
     * @Type("array")
     */
    protected $additional;

    /**
     * Constructor
     * @param mixed[] $data Associated array of property values initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->id = isset($data['id']) ? $data['id'] : null;
        $this->name = isset($data['name']) ? $data['name'] : null;
        $this->type = isset($data['type']) ? $data['type'] : null;
        $this->ageRestriction = isset($data['ageRestriction']) ? $data['ageRestriction'] : null;
        $this->duration = isset($data['duration']) ? $data['duration'] : null;
        $this->tags = isset($data['tags']) ? $data['tags'] : null;
        $this->participants = isset($data['participants']) ? $data['participants'] : null;
        $this->images = isset($data['images']) ? $data['images'] : null;
        $this->videos = isset($data['videos']) ? $data['videos'] : null;
        $this->urls = isset($data['urls']) ? $data['urls'] : null;
        $this->fullDescription = isset($data['fullDescription']) ? $data['fullDescription'] : null;
        $this->briefDescription = isset($data['briefDescription']) ? $data['briefDescription'] : null;
        $this->originalTitle = isset($data['originalTitle']) ? $data['originalTitle'] : null;
        $this->countries = isset($data['countries']) ? $data['countries'] : null;
        $this->productionYear = isset($data['productionYear']) ? $data['productionYear'] : null;
        $this->stageTheatre = isset($data['stageTheatre']) ? $data['stageTheatre'] : null;
        $this->state = isset($data['state']) ? $data['state'] : null;
        $this->releaseDate = isset($data['releaseDate']) ? $data['releaseDate'] : null;
        $this->nonStop = isset($data['nonStop']) ? $data['nonStop'] : null;
        $this->organizer = isset($data['organizer']) ? $data['organizer'] : null;
        $this->permanent = isset($data['permanent']) ? $data['permanent'] : null;
        $this->sourceLink = isset($data['sourceLink']) ? $data['sourceLink'] : null;
        $this->ratings = isset($data['ratings']) ? $data['ratings'] : null;
        $this->synonyms = isset($data['synonyms']) ? $data['synonyms'] : null;
        $this->integrations = isset($data['integrations']) ? $data['integrations'] : null;
        $this->additional = isset($data['additional']) ? $data['additional'] : null;
    }

    /**
     * Gets id.
     *
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets id.
     *
     * @param string|null $id
     *
     * @return $this
     */
    public function setId($id = null)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Gets name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets name.
     *
     * @param string|null $name
     *
     * @return $this
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets type.
     *
     * @return \YagaSchedule\Server\Model\EventType|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets type.
     *
     * @param YagaSchedule\Server\Model\EventType|null $type
     *
     * @return $this
     */
    public function setType(EventType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets ageRestriction.
     *
     * @return int|null
     */
    public function getAgeRestriction()
    {
        return $this->ageRestriction;
    }

    /**
     * Sets ageRestriction.
     *
     * @param int|null $ageRestriction
     *
     * @return $this
     */
    public function setAgeRestriction($ageRestriction = null)
    {
        $this->ageRestriction = $ageRestriction;

        return $this;
    }

    /**
     * Gets duration.
     *
     * @return int|null
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Sets duration.
     *
     * @param int|null $duration
     *
     * @return $this
     */
    public function setDuration($duration = null)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Gets tags.
     *
     * @return \YagaSchedule\Server\Model\Tag[]|null
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Sets tags.
     *
     * @param YagaSchedule\Server\Model\Tag[]|null $tags
     *
     * @return $this
     */
    public function setTags(Tag $tags = null)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Gets participants.
     *
     * @return \YagaSchedule\Server\Model\Participant[]|null
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * Sets participants.
     *
     * @param YagaSchedule\Server\Model\Participant[]|null $participants
     *
     * @return $this
     */
    public function setParticipants(Participant $participants = null)
    {
        $this->participants = $participants;

        return $this;
    }

    /**
     * Gets images.
     *
     * @return \YagaSchedule\Server\Model\Image[]|null
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets images.
     *
     * @param YagaSchedule\Server\Model\Image[]|null $images
     *
     * @return $this
     */
    public function setImages(Image $images = null)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Gets videos.
     *
     * @return \YagaSchedule\Server\Model\Video[]|null
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * Sets videos.
     *
     * @param YagaSchedule\Server\Model\Video[]|null $videos
     *
     * @return $this
     */
    public function setVideos(Video $videos = null)
    {
        $this->videos = $videos;

        return $this;
    }

    /**
     * Gets urls.
     *
     * @return string[]|null
     */
    public function getUrls()
    {
        return $this->urls;
    }

    /**
     * Sets urls.
     *
     * @param string[]|null $urls
     *
     * @return $this
     */
    public function setUrls($urls = null)
    {
        $this->urls = $urls;

        return $this;
    }

    /**
     * Gets fullDescription.
     *
     * @return string|null
     */
    public function getFullDescription()
    {
        return $this->fullDescription;
    }

    /**
     * Sets fullDescription.
     *
     * @param string|null $fullDescription
     *
     * @return $this
     */
    public function setFullDescription($fullDescription = null)
    {
        $this->fullDescription = $fullDescription;

        return $this;
    }

    /**
     * Gets briefDescription.
     *
     * @return string|null
     */
    public function getBriefDescription()
    {
        return $this->briefDescription;
    }

    /**
     * Sets briefDescription.
     *
     * @param string|null $briefDescription
     *
     * @return $this
     */
    public function setBriefDescription($briefDescription = null)
    {
        $this->briefDescription = $briefDescription;

        return $this;
    }

    /**
     * Gets originalTitle.
     *
     * @return string|null
     */
    public function getOriginalTitle()
    {
        return $this->originalTitle;
    }

    /**
     * Sets originalTitle.
     *
     * @param string|null $originalTitle
     *
     * @return $this
     */
    public function setOriginalTitle($originalTitle = null)
    {
        $this->originalTitle = $originalTitle;

        return $this;
    }

    /**
     * Gets countries.
     *
     * @return string[]|null
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Sets countries.
     *
     * @param string[]|null $countries
     *
     * @return $this
     */
    public function setCountries($countries = null)
    {
        $this->countries = $countries;

        return $this;
    }

    /**
     * Gets productionYear.
     *
     * @return int|null
     */
    public function getProductionYear()
    {
        return $this->productionYear;
    }

    /**
     * Sets productionYear.
     *
     * @param int|null $productionYear
     *
     * @return $this
     */
    public function setProductionYear($productionYear = null)
    {
        $this->productionYear = $productionYear;

        return $this;
    }

    /**
     * Gets stageTheatre.
     *
     * @return string|null
     */
    public function getStageTheatre()
    {
        return $this->stageTheatre;
    }

    /**
     * Sets stageTheatre.
     *
     * @param string|null $stageTheatre
     *
     * @return $this
     */
    public function setStageTheatre($stageTheatre = null)
    {
        $this->stageTheatre = $stageTheatre;

        return $this;
    }

    /**
     * Gets state.
     *
     * @return string|null
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Sets state.
     *
     * @param string|null $state
     *
     * @return $this
     */
    public function setState($state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Gets releaseDate.
     *
     * @return string|null
     */
    public function getReleaseDate()
    {
        return $this->releaseDate;
    }

    /**
     * Sets releaseDate.
     *
     * @param string|null $releaseDate
     *
     * @return $this
     */
    public function setReleaseDate($releaseDate = null)
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    /**
     * Gets nonStop.
     *
     * @return bool|null
     */
    public function isNonStop()
    {
        return $this->nonStop;
    }

    /**
     * Sets nonStop.
     *
     * @param bool|null $nonStop
     *
     * @return $this
     */
    public function setNonStop($nonStop = null)
    {
        $this->nonStop = $nonStop;

        return $this;
    }

    /**
     * Gets organizer.
     *
     * @return string|null
     */
    public function getOrganizer()
    {
        return $this->organizer;
    }

    /**
     * Sets organizer.
     *
     * @param string|null $organizer
     *
     * @return $this
     */
    public function setOrganizer($organizer = null)
    {
        $this->organizer = $organizer;

        return $this;
    }

    /**
     * Gets permanent.
     *
     * @return bool|null
     */
    public function isPermanent()
    {
        return $this->permanent;
    }

    /**
     * Sets permanent.
     *
     * @param bool|null $permanent
     *
     * @return $this
     */
    public function setPermanent($permanent = null)
    {
        $this->permanent = $permanent;

        return $this;
    }

    /**
     * Gets sourceLink.
     *
     * @return string|null
     */
    public function getSourceLink()
    {
        return $this->sourceLink;
    }

    /**
     * Sets sourceLink.
     *
     * @param string|null $sourceLink
     *
     * @return $this
     */
    public function setSourceLink($sourceLink = null)
    {
        $this->sourceLink = $sourceLink;

        return $this;
    }

    /**
     * Gets ratings.
     *
     * @return \YagaSchedule\Server\Model\Rating[]|null
     */
    public function getRatings()
    {
        return $this->ratings;
    }

    /**
     * Sets ratings.
     *
     * @param YagaSchedule\Server\Model\Rating[]|null $ratings
     *
     * @return $this
     */
    public function setRatings(Rating $ratings = null)
    {
        $this->ratings = $ratings;

        return $this;
    }

    /**
     * Gets synonyms.
     *
     * @return string[]|null
     */
    public function getSynonyms()
    {
        return $this->synonyms;
    }

    /**
     * Sets synonyms.
     *
     * @param string[]|null $synonyms
     *
     * @return $this
     */
    public function setSynonyms($synonyms = null)
    {
        $this->synonyms = $synonyms;

        return $this;
    }

    /**
     * Gets integrations.
     *
     * @return \YagaSchedule\Server\Model\SourceRef[]|null
     */
    public function getIntegrations()
    {
        return $this->integrations;
    }

    /**
     * Sets integrations.
     *
     * @param YagaSchedule\Server\Model\SourceRef[]|null $integrations
     *
     * @return $this
     */
    public function setIntegrations(SourceRef $integrations = null)
    {
        $this->integrations = $integrations;

        return $this;
    }

    /**
     * Gets additional.
     *
     * @return array|null
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * Sets additional.
     *
     * @param array|null $additional
     *
     * @return $this
     */
    public function setAdditional(array $additional = null)
    {
        $this->additional = $additional;

        return $this;
    }
}



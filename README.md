# SwaggerServer
Common schema for Yaga schedule requests. Swagger

This [Symfony](https://symfony.com/) bundle is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 18.40.0
- Build package: io.swagger.codegen.languages.SymfonyServerCodegen

## Requirements

PHP 5.4.0 and later

## Installation & Usage

To install the dependencies via [Composer](http://getcomposer.org/), add the following repository to `composer.json` of your Symfony project:

```json
{
    "repositories": [{
        "type": "path",
        "url": "//Path to your generated swagger bundle"
    }],
}
```

Then run:

```
composer require swagger/server-bundle:dev-master
```

to add the generated swagger bundle as a dependency.

## Tests

To run the unit tests for the generated bundle, first navigate to the directory containing the code, then run the following commands:

```
composer install
./vendor/bin/phpunit
```


## Getting Started

Step 1: Please follow the [installation procedure](#installation--usage) first.

Step 2: Enable the bundle in the kernel:

```php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = array(
        // ...
        new YagaSchedule\Server\SwaggerServerBundle(),
        // ...
    );
}
```

Step 3: Register the routes:

```yaml
# app/config/routing.yml
swagger_server:
    resource: "@SwaggerServerBundle/Resources/config/routing.yml"
```

Step 4: Implement the API calls:

```php
<?php
// src/Acme/MyBundle/Api/YagaScheduleApiInterface.php

namespace Acme\MyBundle\Api;

use YagaSchedule\Server\Api\YagaScheduleApiInterface;

class YagaScheduleApi implements YagaScheduleApiInterface // An interface is autogenerated
{

    // Other operation methods ...
}
```

Step 5: Tag your API implementation:

```yaml
# src/Acme/MyBundle/Resources/services.yml
services:
    # ...
    acme.my_bundle.api.yagaSchedule:
        class: Acme\MyBundle\Api\YagaScheduleApi
        tags:
            - { name: "swagger_server.api", api: "yagaSchedule" }
    # ...
```

Now you can start using the bundle!


## Documentation for API Endpoints

All URIs are relative to *https://localhost*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*YagaScheduleApiInterface* | [**getCities**](Resources/docs/Api/YagaScheduleApiInterface.md#getcities) | **GET** /cities | 
*YagaScheduleApiInterface* | [**getEvents**](Resources/docs/Api/YagaScheduleApiInterface.md#getevents) | **GET** /events | 
*YagaScheduleApiInterface* | [**getHallplan**](Resources/docs/Api/YagaScheduleApiInterface.md#gethallplan) | **GET** /hallplan | 
*YagaScheduleApiInterface* | [**getHalls**](Resources/docs/Api/YagaScheduleApiInterface.md#gethalls) | **GET** /halls | 
*YagaScheduleApiInterface* | [**getManifest**](Resources/docs/Api/YagaScheduleApiInterface.md#getmanifest) | **GET** /manifest | 
*YagaScheduleApiInterface* | [**getOrganizers**](Resources/docs/Api/YagaScheduleApiInterface.md#getorganizers) | **GET** /organizers | 
*YagaScheduleApiInterface* | [**getPersons**](Resources/docs/Api/YagaScheduleApiInterface.md#getpersons) | **GET** /persons | 
*YagaScheduleApiInterface* | [**getSchedule**](Resources/docs/Api/YagaScheduleApiInterface.md#getschedule) | **GET** /schedule | 
*YagaScheduleApiInterface* | [**getVenues**](Resources/docs/Api/YagaScheduleApiInterface.md#getvenues) | **GET** /venues | 


## Documentation For Models

 - [CancelAllowance](Resources/docs/Model/CancelAllowance.md)
 - [City](Resources/docs/Model/City.md)
 - [Coordinates](Resources/docs/Model/Coordinates.md)
 - [Event](Resources/docs/Model/Event.md)
 - [EventType](Resources/docs/Model/EventType.md)
 - [GetCitiesResult](Resources/docs/Model/GetCitiesResult.md)
 - [GetEventsResult](Resources/docs/Model/GetEventsResult.md)
 - [GetHallplanResult](Resources/docs/Model/GetHallplanResult.md)
 - [GetHallsResult](Resources/docs/Model/GetHallsResult.md)
 - [GetManifestResult](Resources/docs/Model/GetManifestResult.md)
 - [GetOrganizersResult](Resources/docs/Model/GetOrganizersResult.md)
 - [GetPersonsResult](Resources/docs/Model/GetPersonsResult.md)
 - [GetScheduleResult](Resources/docs/Model/GetScheduleResult.md)
 - [GetVenuesResult](Resources/docs/Model/GetVenuesResult.md)
 - [Hall](Resources/docs/Model/Hall.md)
 - [Image](Resources/docs/Model/Image.md)
 - [Level](Resources/docs/Model/Level.md)
 - [ListInfo](Resources/docs/Model/ListInfo.md)
 - [Manifest](Resources/docs/Model/Manifest.md)
 - [Money](Resources/docs/Model/Money.md)
 - [MovieFormat](Resources/docs/Model/MovieFormat.md)
 - [Organizer](Resources/docs/Model/Organizer.md)
 - [Participant](Resources/docs/Model/Participant.md)
 - [Person](Resources/docs/Model/Person.md)
 - [PersonType](Resources/docs/Model/PersonType.md)
 - [Phone](Resources/docs/Model/Phone.md)
 - [PreOrderAllowance](Resources/docs/Model/PreOrderAllowance.md)
 - [Rating](Resources/docs/Model/Rating.md)
 - [Role](Resources/docs/Model/Role.md)
 - [Seat](Resources/docs/Model/Seat.md)
 - [Session](Resources/docs/Model/Session.md)
 - [SessionTime](Resources/docs/Model/SessionTime.md)
 - [SessionType](Resources/docs/Model/SessionType.md)
 - [SourceRef](Resources/docs/Model/SourceRef.md)
 - [Tag](Resources/docs/Model/Tag.md)
 - [Venue](Resources/docs/Model/Venue.md)
 - [VenueType](Resources/docs/Model/VenueType.md)
 - [Video](Resources/docs/Model/Video.md)
 - [WorkTime](Resources/docs/Model/WorkTime.md)


## Documentation For Authorization

 All endpoints do not require authorization.


## Author




